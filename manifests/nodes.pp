# hh-web, hh-db, lfsg-web, lfsg-db, oei-web, oei-db, locals-db, ei, belhaven, pp-db
node
  '500127-hhc5web',
  '500128-hhc5db',
  '500130-lfrc5web',
  '500131-lfrc5db',
  '500125-oeic5web',
  '500126-oeic5db',
  '500133-locc5db',
  '500129-eic5webdb',
  '500134-bhc5webdb',
  '600719-pp-db'
{
  class { 'system::profile': }
  class { 'users::gkdigital': }
  class { 'users::deployment': }
  class { 'users::mobas': }
}

# locals-web
node '500132-locc5web' {
  class { 'system::profile': }
  class { 'users::gkdigital': }
  class { 'users::deployment': }
  class { 'users::mobas': }

  group { 'sftp':
    ensure => present,
  }

  user { 'gkolpay':
    ensure     => present,
    comment    => 'GK Online Payments',
    home       => '/srv/http/www.greenekingonlinepayments.co.uk/files',
    managehome => true,
    password   => '$6$ibuaxlBr92iVMEQW$ysP0CD/Q7o5B4zIFobAytLCWcscTw3zY5R/1SOIiGk1zIU9nI.KZFEip0WOAaR87s/jMka4KDuDIzRyE8OoEQ1',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
}

# pp-web
node '600716-pp-web' {
  class { 'system::profile': }
  class { 'users::gkdigital': }
  class { 'users::deployment': }
  class { 'users::mobas': }

  user { 'campaignworks':
    ensure     => present,
    comment    => 'Campaign Works',
    home       => '/srv/build/campaignworks',
    managehome => true,
    password   => '$6$Sx2i4B731YnVesOU$4nPnwgMQhMfFeL1wMoBIcUy3VBVX5zpTeVb7DvrGfeI864Lx30I/Awwa6CK0U2gqhX296IQhGTrSdmGnW8zOo/',
    shell      => '/usr/bin/lshell',
  }

  user { 'thebigkick':
    ensure     => present,
    comment    => 'The Big Kick',
    home       => '/srv/build/thebigkick',
    managehome => true,
    password   => '$6$MhAx4YlnN4c/KIiS$tbv0/Wm7f0zU1isTBaaCk3JKhkMWFzUy6xtxMt0/Cl3sNOVUA3My1bd.QyhiVXqqQT5yf1IMGaCSeZeBg/C1v1',
    shell      => '/usr/bin/lshell',
  }
}

# bb
node '500136-bbc5webdb' {
  class { 'system::profile': }
  class { 'users::gkdigital': }
  class { 'users::deployment': }
  class { 'users::mobas': }

  group { 'sftp':
    ensure => present,
  }

  user { 'gkheretohelp':
    ensure     => present,
    comment    => 'GK Here to Help',
    home       => '/srv/http/www.greeneking-heretohelp.co.uk/sftp',
    managehome => true,
    password   => '$6$6Clti/lHGaKnnGFJ$Ytgkk9uHA3Snde9mYI/LuaGwTg.AX0hMOPZTV39MalUXz/jNxU2/K952ZJYDyLIbppseLBF8KkGx1hmfOqWt41',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  ssh_authorized_key { 'gkheretohelp_gkbiadmin':
    user => 'gkheretohelp',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAu0zxy5s/s/Bhp3RZyD6qu69c5VXy5F6aakq8UYx/0RZn6HtV0JmOpssWKCYUBidy145p9FowsBOmNzaDZ6q811Q6KyMAcDrjXiUgGBvepttfpyI58mu+QG+XBF7IHpdM1zJD/QM39vR2SGDh7VcU4DT1pKSs/fjC+bYgS1t+B9+EIxPMnmVvCHUxMfdoxWcX4TNBIA3EEts+hcVp2bsL+NEJpUR2rS1nKF4N4ZAelR4NwvXOs+0Vd874vILTbwLpG8qOhgmO/NtpC6JxO9IfXsqqbixu+H3UjkImC5/GMyRUNac1oaOfWkc/4non/pocoHijiJ1hB3tGwqWTADI1oQ==',
  }

  user { 'gkshop':
    ensure     => present,
    comment    => 'GK Shop',
    home       => '/var/www/vhosts/www.greenekingshop.co.uk',
    managehome => true,
    password   => '$6$pq2U2L8gSr7rgEa3$OtA5h0Cu/XPV2dTPeJPq1vhgbglaomfR9heqHfIXr2T9EO3hWJcPVPIL215Ckz7bAb3tvM.x93ZgEPGTwYvfD/',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }

  user { 'belhavenshop':
    ensure     => present,
    comment    => 'Belhaven Shop',
    home       => '/srv/http/belhaven.greenekingshop.co.uk',
    managehome => true,
    password   => '$6$AvsPcykK183w52CV$u48qFZF1Mlng.Pl91u3XJpKWvsBdWxohJnv1X2xRaOr.DDU1BJ6FVRyjBoW09jwgsT/0H8HsIX/HLezBNOpmf/',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }

  user { 'gksummerfest':
    ensure     => present,
    comment    => 'GK Summerfest',
    home       => '/srv/http/www.greenekingsummerfest.co.uk',
    managehome => true,
    password   => '$6$5lsgzkrnHaxgh1RD$5UbMUooBPDgRJIXSo.4MVea1jANhOKYWHTSWDAQWTWBSQ0bq2CRM4O/WxyoP/JxLIWXK1k4pAEkBZyzN4Sdif0',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }

  user { 'browsethemenu':
    ensure     => present,
    comment    => 'Browse the Menu',
    home       => '/srv/http/www.browsethemenu.co.uk',
    managehome => true,
    password   => '$6$aR7Eb/BicByRxuMJ$jpiFuzX2md3krZomJWf1jBFhIvjj21ksZTkW2oLzSe8OW0AZHFiaXPvRq3W.b6rmxquefilR/S62Rl4LeoXym0',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
}

# stage
node '500135-flc5webdb' {
  class { 'system::profile': }
  class { 'system::hosts': }

  class { 'users::gkdigital': }
  class { 'users::deployment': }
  class { 'users::mobas': }

  # Drush site aliases.
  file { '/etc/drush/aliases.drushrc.php':
    source => '/srv/puppet/files/drush/aliases/aliases.drushrc.php',
  }
  file { '/etc/drush/example.projects.aliases.drushrc.php':
    source => '/srv/puppet/files/drush/aliases/example.projects.aliases.drushrc.php',
  }
  file { '/etc/drush/example.settings.aliases.drushrc.php':
    source => '/srv/puppet/files/drush/aliases/example.settings.aliases.drushrc.php',
  }

  # Drush site-sync.
  file { '/usr/local/bin/drush-site-sync':
    source => '/srv/puppet/files/drush/drush-site-sync',
    mode => '0755',
  }

  #
  # SFTP
  #
  group { 'sftp':
    ensure => present,
  }

  # User: bigreeneking
  user { 'bigreeneking':
    ensure     => present,
    comment    => 'Business Intelligence',
    home       => '/srv/ftp/bigreeneking',
    managehome => true,
    password   => '$6$C94A4NKg8KsZIxyT$9fjeuYy9uo8OmbqKQDwBc6CMgWtUqhEQVDQPHZ3j2oldhvNIm7ntq5TUtI2O6PBW43qIiSBqml3550caXBlD9.',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  ssh_authorized_key { 'bigreeneking':
    user => 'bigreeneking',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAu0zxy5s/s/Bhp3RZyD6qu69c5VXy5F6aakq8UYx/0RZn6HtV0JmOpssWKCYUBidy145p9FowsBOmNzaDZ6q811Q6KyMAcDrjXiUgGBvepttfpyI58mu+QG+XBF7IHpdM1zJD/QM39vR2SGDh7VcU4DT1pKSs/fjC+bYgS1t+B9+EIxPMnmVvCHUxMfdoxWcX4TNBIA3EEts+hcVp2bsL+NEJpUR2rS1nKF4N4ZAelR4NwvXOs+0Vd874vILTbwLpG8qOhgmO/NtpC6JxO9IfXsqqbixu+H3UjkImC5/GMyRUNac1oaOfWkc/4non/pocoHijiJ1hB3tGwqWTADI1oQ==',
  }
  file { '/srv/ftp/bigreeneking':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/bigreeneking/files':
    ensure => directory,
    mode => '2775',
    owner => 'bigreeneking',
    group => 'sftp',
  }

  # User: gi
  user { 'gi':
    ensure     => present,
    comment    => 'GI',
    home       => '/srv/ftp/gi',
    managehome => true,
    password   => '$6$JwpvYJ8A4ChKmklC$It2cs9WdwjYyFEkn7kdg3xMF2c/qXvUnW8An.6e2oo0gnVOKgKXV/wmQr0T1kXhvX0iT7xfjvX7PzdyONWeDc.',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/gi':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/gi/files':
    ensure => directory,
    mode => '2775',
    owner => 'gi',
    group => 'sftp',
  }

  # User: givex
  user { 'givex':
    ensure     => present,
    comment    => 'Givex',
    home       => '/srv/ftp/givex',
    managehome => true,
    password   => '$6$wsekh1gm2.NZH2sO$rFOYRUBh3ZQENllOwqKecWQw7cnGvFNgHXp2.2jbvOL3jRR8bNhyxOblwFMDwsH5914CZzsWtMeLoIG2m0pbB.',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/givex':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/givex/files':
    ensure => directory,
    mode => '2775',
    owner => 'givex',
    group => 'sftp',
  }

  # User: gkfiles
  user { 'gkfiles':
    ensure     => present,
    comment    => 'GK Files',
    home       => '/srv/ftp/gkfiles',
    managehome => true,
    password   => '$6$jrwweDtf5TFV9GwH$Bij31zhUNE4tA/qjpbaeFvGKWzKRzHki5uhT4MQg2k6A.BzI/XSHtIdisY2kKRqRojdt4f/CuKmQbcHzspIkS1',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/gkfiles':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }

  # User: greeneking
  user { 'greeneking':
    ensure     => present,
    comment    => 'Greene King',
    home       => '/srv/ftp/greeneking',
    managehome => true,
    password   => '$6$VaXbVOURyHVqedNs$ef2Jf2AWhk0tjFNsV3f0LSIQynCBDpJ0CHaBeWRZhiCCoM1YHQQv7GsIB7QxjDEdB.Wop2Ldw3IwcvEep7Rsd0',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/greeneking':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/greeneking/files':
    ensure => directory,
    mode => '2775',
    owner => 'greeneking',
    group => 'sftp',
  }

  # User: livebookings
  user { 'livebookings':
    ensure     => present,
    comment    => 'Live Bookings',
    home       => '/srv/ftp/livebookings',
    managehome => true,
    password   => '$6$v9PhnI4GAXelZy4m$.jOYSnXxCFjCmYqFyTJWVtq/BQzq0UliKD5OuaDZsdVO.ViX0HpJLYMk/Dnt1rRqL9oBOpIcJgJqQkwyAvRfG.',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/livebookings':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/livebookings/files':
    ensure => directory,
    mode => '2775',
    owner => 'livebookings',
    group => 'sftp',
  }

  # User: logwood
  user { 'logwood':
    ensure     => present,
    comment    => 'Logwood',
    home       => '/srv/ftp/logwood',
    managehome => true,
    password   => '$6$C5dAW7MzjpBaEwpt$g1WMvRzTL8czQO/7PiafIAX109t2K5Mh8vtIq4udqzu6xRUNUQa9gfrrMP4h58wNuyXWpMRNRGQhVV8lNqrdG1',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/logwood':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/logwood/files':
    ensure => directory,
    mode => '2775',
    owner => 'logwood',
    group => 'sftp',
  }

  # User: marketforce
  user { 'marketforce':
    ensure     => present,
    comment    => 'Market Force',
    home       => '/srv/ftp/marketforce',
    managehome => true,
    password   => '$6$NRw95qR9YfSAXtla$BEdlxjwd36Gxz7O9prHk7y.6N2NH6nd6KxMf.pZERKFmRpEpszFzwyd8YGl913L8UztaLDRQgZNBMTQUiHc05/',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/marketforce':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/marketforce/files':
    ensure => directory,
    mode => '2775',
    owner => 'marketforce',
    group => 'sftp',
  }

  # User: mobasftp
  user { 'mobasftp':
    ensure     => present,
    comment    => 'Mobas FTP',
    home       => '/srv/ftp/mobasftp',
    managehome => true,
    password   => '$6$BcC0bde6VM6hmFh0$fGODAg3NLfvwi5vzUBjHB5uTHa2jrfnG0gl.udLeOD48qW37EQBocPTcoJy185MGd65kkVoMq9/lqxna4Bkc41',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/mobasftp':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/mobasftp/files':
    ensure => directory,
    mode => '2775',
    owner => 'mobasftp',
    group => 'sftp',
  }

  # User: rezlynx
  user { 'rezlynx':
    ensure     => present,
    comment    => 'Rezlynx',
    home       => '/srv/ftp/rezlynx',
    managehome => true,
    password   => '$6$1Myr02f1JRIaRYQG$0h0wt00/8UN1YMDLJ9fOSTJwEwSo.RHmEPuPDA7u43heU0ETef35rU/JEpBKN0rWt8cpDKpfVt/fE6gCfXJfM/',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/rezlynx':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/rezlynx/files':
    ensure => directory,
    mode => '2775',
    owner => 'rezlynx',
    group => 'sftp',
  }

  # User: sky
  user { 'sky':
    ensure     => present,
    comment    => 'Sky',
    home       => '/srv/ftp/sky',
    managehome => true,
    password   => '$6$aKpt4OYfQFphcGmF$un8veJp15pVKr45j8K/ErrC45OwI1tjKnWUenAX7RU6Hhryv/P5xhlpIL4euYe3Y5UtFF1XUVvB6U9wDtj5sT/',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/sky':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/sky/files':
    ensure => directory,
    mode => '2775',
    owner => 'sky',
    group => 'sftp',
  }

  # User: tangent
  user { 'tangent':
    ensure     => present,
    comment    => 'Tangent',
    home       => '/srv/ftp/tangent',
    managehome => true,
    password   => '$6$VIMvH63mfZnGWDEj$CtyXeBHsffBoVkRDuIg0Ax4GZC37pEtXJGJYsmYP93.ysheYnH7a5c61H/LqFWqmKlOYdX4BgMzyGjqTx0YUr0',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/tangent':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/tangent/files':
    ensure => directory,
    mode => '2775',
    owner => 'tangent',
    group => 'sftp',
  }

  # User: txd
  user { 'txd':
    ensure     => present,
    comment    => 'TXD',
    home       => '/srv/ftp/txd',
    managehome => true,
    password   => '$6$9faN5x/9GuQ1iUIt$GGoq8z0.B82MqnZT8J5LEb/XQeQ7jeFMa6520sp9GH3n.a70lX.5mAOi/cuo0Ul6wEPc1Nocy5PKByFEtmbsq1',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/txd':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/txd/files':
    ensure => directory,
    mode => '2775',
    owner => 'txd',
    group => 'sftp',
  }

  # User: hopewiser
  user { 'hopewiser':
    ensure     => present,
    comment    => 'Hopewiser',
    home       => '/srv/ftp/hopewiser',
    managehome => true,
    password   => '$6$4KIBO.hqBsqs8ndd$WYNU9M0wPjOh/rlu8yu4I4elyi1fu0yi0NtjsHGSWhc8OsnXQqdMu0ZHiFQ6.b1wmU8VmoeByHdTAx3pgGlz8/',
    groups     => ['sftp'],
    shell      => '/sbin/nologin',
    require    => Group['sftp'],
  }
  file { '/srv/ftp/hopewiser':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'sftp',
  }
  file { '/srv/ftp/hopewiser/files':
    ensure => directory,
    mode => '2775',
    owner => 'hopewiser',
    group => 'sftp',
  }
}
