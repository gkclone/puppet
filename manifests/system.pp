class system::profile {
  file { '/etc/profile.d/colours.sh':
    source => '/srv/puppet/files/colours.sh',
  }

  file { '/etc/profile.d/motd.sh':
    source => '/srv/puppet/files/motd.sh',
  }

  file { '/etc/profile.d/prompt.sh':
    source => '/srv/puppet/files/prompt.sh',
  }

  file { '/usr/local/bin/git-modules-status':
    source => '/srv/puppet/files/git-modules-status',
    mode => '0755',
  }

  file { '/usr/local/bin/drush-fd-overridden':
    source => '/srv/puppet/files/drush/drush-fd-overridden',
    mode => '0755',
  }
}

class system::hosts {
  host { 'stage':
    ip => '192.168.200.24',
  }
  host { 'hh-web':
    ip => '192.168.200.30',
  }
  host { 'hh-db':
    ip => '192.168.100.16',
  }
  host { 'lfsg-web':
    ip => '192.168.200.18',
  }
  host { 'lfsg-db':
    ip => '192.168.100.19',
  }
  host { 'locals-web':
    ip => '192.168.200.20',
  }
  host { 'locals-db':
    ip => '192.168.100.21',
  }
  host { 'bb':
    ip => '192.168.200.27',
  }
}
