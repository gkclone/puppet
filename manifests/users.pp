class users::gkdigital {
  user { 'tom':
    ensure     => present,
    comment    => 'Tom Cant',
    home       => '/home/tom',
    managehome => true,
    groups     => ['wheel'],
  }
  ssh_authorized_key { 'tom_work':
    user => 'tom',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAzu+5frfGLM6dmWYZL1OcvHIWk/02q2sWD7+2xK9gtq4Z1qwCZE0M6Na4ozF4/k8VSUaoAvhl0FMZnTNAW3Wg2/Jls4ImcThDXqhOH+3da59UT1RyRSn+YWs2jve0u/QGujhWmwBMCRVIfp2lAmYeD7OdCuIOGXi5v35Wf0VJCMPb6xvj4q65cCARmZseFmlOkLzVR9LInkNHz1jyGmTDEdShvwUWOQzQEow/BkMi928NDhv9lA9SV5lbOdR+gyPjpLJPDrr7S6QhUiGd8bbEE36xEEfyIxzRWm8FKREPPGrmZyBNa9ZunXRGc9w9jZllW+GfkVRXiX1jFaB/8xCr4Q==',
  }
  ssh_authorized_key { 'tom_home':
    user => 'tom',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC4agiC2JdSRJ1EadcJOvvSLK+1iE5jF8Hk6NUzMJKJaIsTL9UAftW03Xs/+zf01ra7VhSy5QpYEHS+T7jN+d0DDEdNIQjJHvILohypPfCQ8RxjVTN5Kozy8n3zNrnpdOh5rwBZHBxC1+1VW3sIkad4eRy8QsjdiRu2mDgCFwGmL4YBx0VrbgxjY5/V66IeX2V6IjtSPCYajWL1cg0GwXsPq4dzMz4hqp64kVA7YTO3euzx4gwRZQoyjc7Iala3H8+PbDNSapyUNYjBTzxttFv6u3hKmlbYNrch7r/2NomtbY+DdaFGlFuINichOAhN9iDRsrx/70nDxvSh2uuq196f',
  }

  user { 'phil':
    ensure     => present,
    comment    => 'Phil Maurer',
    home       => '/home/phil',
    managehome => true,
    groups     => ['wheel'],
  }
  ssh_authorized_key { 'phil_work':
    user => 'phil',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDAMVDuMHJcjbtbLh/3EqOH4/6ThvK1oZXDGqHfvx77pXCwOq+h6ceXOKwPSulAxowpqqiTHJtHA02D0xPUsdPku0J1Pwc3XKobdhIcuIcrvEI5vXqRX3ReE2qS7lZeYpWmrpq3xEFDhhhdu1rsFZej4TbFZWG6a1h8wLMJWaQfXUmwU0b7pQm9ZQbZVQ9iwexZEbmNIyUwNmFx2vsoZFIhULi1sLzDY9ZjuWlybMu5Qr/63FJLEq2kYOnO3br0fMIb8lGHG7sHwP8+aUN+ab1qudYHRCUtjxCb336hpX0MdaoLeALX/36iSHBuTCQ94efA5oUkiM8ZOZm+fQ89Pa03',
  }

  user { 'ian':
    ensure     => present,
    comment    => 'Ian Pettitt',
    home       => '/home/ian',
    managehome => true,
    groups     => ['wheel'],
  }
  ssh_authorized_key { 'ian_work':
    user => 'ian',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQChHT/n1hCKhXeKRUqSzN6YZQGwHT9P81NX4+oGwzne4FtI1CqCNnb/C3Yf/bX+ilNSBEOGZNbr8LSvrYeGsT0GE51SkLToCiqW9S6RMsqdTkR6m2Q5uRLXICDMdZCwlPQQSHkWixNhHkno/+mUwZJfjQB2DOAMly2xu3OhOSM8rWoSy+P0QSFMdF/M+KmFtFZTOxsGXCCWnQg4AwkOoExBzG9SC45WNrURjJeB4UJ2+Kk2jKKLZF0GVcD0EReDSAznBSAOcf6TmkiRUZ1FXWgTvw/gWcaYZdcTyaAABpch0ficXAqj8kUuYBgj2zmllN84g3aHPrteAfG7zhzUmL5v',
  }
}

class users::deployment {
  user { 'drush':
    ensure     => present,
    comment    => 'Drush',
    home       => '/home/drush',
    managehome => true,
  }
  ssh_authorized_key { 'drush':
    user => 'drush',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAoijVzSohGUYiFX2naCbUZRVd7V2te0eQoyRlOiRcVgkgQRduKdQQ3tTHDntUwFpkWQNljsfhkk3e+KdjT1Isjcc9BHG7JowDxJ2vT4APxRlilXtUIyzt6HSUFXgZJcWUvvH2IbbeSt0jkB298bdpw4nOKc7LtLJqMuxPZvxqe/L/JiB/XW9a3ovpmjWMtfNV8eOZ3n/FIQrZb0pIOkL+NHFE+8eNp8+EKpdCq4kdBWi1L1z1JPqwIKE290LXT++y/0UVGAc1Qu8QnkiAQbuG2HruURzGjigVPuhp3EDeXwyvig2UZZ5FtlbqIuUo4XZjVl6YCmhfKkoEgOSjH26I7w==',
  }
}

class users::mobas {
  user { 'mobas':
    ensure     => present,
    comment    => 'Mobas',
    home       => '/home/mobas',
    managehome => true,
  }
  ssh_authorized_key { 'mobas':
    user => 'mobas',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDFK413IBKUHkrNryHK9Q+fVx0cFzh1RoyKfDSVk5hGrU35pXcwzHkC4JBc1yJR4u1Fx9BfN8q1SEr2JRZFgykQ3VHLoCri4LaFBsIo+Out1jUdgmaQdDqgyk8nDegv1l1oy21aV/MMRdXH2VwR/PWgCBT0x9UKqW16lmZ48YOZDwpUA/SFhUdED0zcQmxOBWlUVm9RoI70tZGtL5aCbzi6nT+NK4Krm/F13f4FX9E5WMvaYIv4XJSMUuKNgHxVPitskkdGmppXHHbGyRRKty7Jb3gyOtzh4+wdrKyV8LUGdAkuUSGHpUljiJ4E7lSq5jLHDqd3qVRh5oLHE1LYlh2D',
  }
}
