<?php

include "projects.aliases.drushrc.php";
include "settings.aliases.drushrc.php";

if (!class_exists('Environment')) {
  // Class used to determine the environment we're running in.
  abstract class Environment {
    const Local = 0;
    const Stage = 1;

    public static function getCurrentEnvironment() {
      // Start by assuming we're running locally.
      $environment = self::Local;

      // Compare this environments hostname to that of stage.
      $hostname = trim(shell_exec('hostname'), "\n");

      if (preg_match('/^500135/', $hostname)) {
        $environment = self::Stage;
      }

      return $environment;
    }
  }
}

$environment = Environment::getCurrentEnvironment();

// Create aliases for each project.
foreach ($projects as $code => $project) {
  // Assume there will always be a 'develop' branch alias.
  $branches = array('develop');

  if (!empty($project['development']['branches'])) {
    $branches = array_merge($branches, $project['development']['branches']);
  }

  // Create an alias for each branch.
  foreach ($branches as $branch) {
    // Alias: local
    if ($environment == Environment::Local) {
      $root = LOCAL_ROOT . "/$code/$branch/webroot";

      if (file_exists($root)) {
        $aliases["$code.local.$branch"] = array(
          'root' => $root,
          'uri' => "$branch.$code." . LOCAL_URI,
        );
      }
    }

    // Alias: stage
    $aliases["$code.stage.$branch"] = array(
      'root' => STAGE_ROOT . "/$code/$branch/webroot",
      'uri' => "$branch.$code." . STAGE_URI,
    );

    if ($environment != Environment::Stage) {
      $aliases["$code.stage.$branch"] += array(
        'remote-host' => 'stage',
        'remote-user' => 'drush',
        'ssh-options' => '-i /home/drush/.ssh/id_rsa',
      );
    }
  }

  // Alias: live
  if (!empty($project['live']['uri']) && !empty($project['live']['host'])) {
    $aliases[$code . '.live'] = array(
      'root' => '/srv/http/' . $project['live']['uri'] . '/webroot',
      'uri' => $project['live']['uri'],
      'remote-host' => $project['live']['host'],
      'remote-user' => 'drush',
      'ssh-options' => '-i /home/drush/.ssh/id_rsa',
    );
  }
}

?>
