#
# Custom prompt command.
#

# Set git autocompletion and PS1 integration
source /etc/bash_completion.d/git
GIT_PS1_SHOWDIRTYSTATE=true
GIT_PS1_DESCRIBE_STYLE=contains

# Configure colours, if available.
if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    RESET="\[${C_RESET}\]"
    C_USER="\[${C_BOLD}${FG_YELLOW}\]"
    C_PATH="\[${C_RESET}${FG_YELLOW}\]"
    C_GIT_CLEAN="\[${C_RESET}${FG_CYAN}\]"
    C_GIT_DIRTY="\[${C_RESET}${FG_MAGENTA}\]"
else
    RESET=
    C_USER=
    C_PATH=
    C_GIT_CLEAN=
    C_GIT_DIRTY=
fi

# Function to assemble the Git part of the prompt.
git_prompt () {
    if ! git rev-parse --git-dir > /dev/null 2>&1; then
        return 0
    fi

    GIT_COLOUR="$C_GIT_CLEAN"

    if git config --get bash.showDirtyState | grep -q true; then
        if ! git diff --quiet 2>/dev/null >&2; then
            GIT_COLOUR="$C_GIT_DIRTY"
        fi
    fi

    echo "${GIT_COLOUR}$(__git_ps1)${C_RESET}"
}

# Thy holy prompt.
PROMPT_COMMAND='PS1="${C_USER}\u${RESET}@${C_USER}\h${RESET}:${C_PATH}\w${RESET}$(git_prompt)\$ "'
