#!/bin/sh

# TODO =========================================================================
# Disk usage
# System load
# Packages to update (security updates)
# Restart required?

#
# Format filesize to a human friendly format e.g. kB, MB, GB.
#
# @param int Filesize to format in bytes
#
filesize_format() {
  awk -v FILESIZE="$1" '
    BEGIN {
      UNITS[1024^3]="Gb";
      UNITS[1024^2]="Mb";
      UNITS[1024]="Kb";

      for (x=1024^3; x>=1024; x/=1024) {
        if (FILESIZE>=x) {
          printf "%.1f %s\n",FILESIZE/x,UNITS[x];
          break;
        }
      }

      if (FILESIZE<1024) print "1kb";
    }
  '
}

# Greeting
HOUR=`date +%H`

if [ $HOUR -lt 12 ]; then
  GREETING="Good morning"
elif [ $HOUR -lt 18 ]; then
  GREETING="Good afternoon"
else
  GREETING="Good evening"
fi

# Memory usage
MEMORY_TOTAL=$(cat /proc/meminfo | grep MemTotal | awk {'print $2'})
MEMORY_FREE=$(cat /proc/meminfo | grep MemFree | awk {'print $2'})
MEMORY_USED=$(($MEMORY_TOTAL-$MEMORY_FREE))
MEMORY_PERCENTAGE=$((100*$MEMORY_USED/$MEMORY_TOTAL))
MEMORY=`filesize_format $(($MEMORY_TOTAL*1000))`

# Swap usage
SWAP_TOTAL=$(cat /proc/meminfo | grep SwapTotal | awk {'print $2'})
SWAP_FREE=$(cat /proc/meminfo | grep SwapFree | awk {'print $2'})
SWAP_PERCENTAGE=$((100*($SWAP_TOTAL-$SWAP_FREE)/$SWAP_TOTAL))
SWAP_USAGE=`filesize_format $(($SWAP_TOTAL*1000))`

# Last login
LOGIN_DATE=$(whoami | last -1 | awk 'NR==1 {print "You last logged in on", $4, $6, $5, "at", $7, "from", $3;}')

# Logged in users
LOGGED_IN_USERS=$(who | cut -d ' ' -f1 | sort | uniq | sed ':a;N;$!ba;s/\n/, /g')
LOGGED_IN_USERS_COUNT=$(echo $LOGGED_IN_USERS | wc -w)

# Styles
LOGO=$C_RESET$FG_MAGENTA
BORDER=$C_RESET$FG_MAGENTA
HEADING=$C_RESET$FG_CYAN
LABEL=$C_RESET$C_BOLD
TEXT=$C_RESET

# Output MOTD
echo -e "
  ${LOGO}  ___ _  __  ___  _      _ _        _
   / __| |/ / |   \(_)__ _(_) |_ __ _| |
  | (_ | ' <  | |) | / _\` | |  _/ _\` | |
   \___|_|\_\ |___/|_\__, |_|\__\__,_|_|
                     |___/

  ${TEXT}${GREETING} `whoami`!

  ${BORDER}+++ ${HEADING}System info ${BORDER}++++++++++++++++++++++++++++++++++++++++++++

  ${LABEL}        Hostname:  ${TEXT}`hostname`
  ${LABEL}              IP:  ${TEXT}$(/sbin/ifconfig eth0 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}')
  ${LABEL}              OS:  ${TEXT}`cat /etc/redhat-release`
  ${LABEL}          Kernel:  ${TEXT}`uname -r`

  ${BORDER}+++ ${HEADING}System status ${BORDER}++++++++++++++++++++++++++++++++++++++++++

  ${LABEL}          Uptime:  ${TEXT}`uptime | sed 's/.*up \([^:]*\):\([^,]*\), .*/\1 hours \2 minutes/'`
  ${LABEL}    Memory usage:  ${TEXT}${MEMORY_PERCENTAGE}% of ${MEMORY}
  ${LABEL}      Swap usage:  ${TEXT}${SWAP_PERCENTAGE}% of ${SWAP_USAGE}
  ${LABEL}       Processes:  $(ps ax | wc -l | tr -d ' ')
  ${LABEL} Logged in users:  ${LOGGED_IN_USERS_COUNT} (${LOGGED_IN_USERS})

  ${BORDER}++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  ${C_RESET}${BOLD}NOTE:${C_RESET} This machine is managed by puppet and as such any
  changes you make are likely to be overridden.

  ${C_RESET}${LOGIN_DATE}

  ${BORDER}++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++${C_RESET}
"
